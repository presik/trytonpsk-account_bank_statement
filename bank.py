# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class Bank(metaclass=PoolMeta):
    __name__ = 'bank'
    account_expense = fields.Many2One('account.account', 'Account')
    list_banks = fields.One2Many('bank.code_relation', 'bank', 'List Banks')
    concepts = fields.One2Many('bank.concept_statement', 'bank', 'Concepts')

    def get_code_bank(self, bank_id):
        bank_code = 0
        for b in self.list_banks:
            if b.relation_bank.id == bank_id:
                bank_code = b.code
                break
        return bank_code


class BankAccount(metaclass=PoolMeta):
    __name__ = 'bank.account'
    account = fields.Many2One('account.account', 'Account',
        required=False, domain=[
            ('type', '!=', None),
            ('bank_reconcile', '=', True),
        ])


class BankCodeRelation(ModelSQL, ModelView):
    "Bank Code Relation"
    __name__ = 'bank.code_relation'

    bank = fields.Many2One('bank', 'Bank')
    code = fields.Char('Code', states={'required': True})
    relation_bank = fields.Many2One('bank', 'Relation Bank', states={'required': True})


class BankConceptStatement(ModelSQL, ModelView):
    "Bank Concept Statement"
    __name__ = 'bank.concept_statement'

    bank = fields.Many2One('bank', 'Bank')
    concept = fields.Char('Concept', states={'required': True})
    description = fields.Char('Description', states={'required': True})
    account = fields.Many2One('account.account', 'Account',
        domain=[('type', '!=', None)])
    debit_credit = fields.Selection([(None, ''), ('debit', 'Debit'), ('credit', 'Credit')],
        'Debit Credit', states={
            'invisible': ~Eval('account', False),
            'required': Eval('account', False),
        })
