# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import datetime
import re
from decimal import Decimal

import polars as pl
from dateutil.relativedelta import relativedelta
from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.model.exceptions import AccessError
from trytond.pool import Pool
from trytond.pyson import Equal, Eval, If, Not
from trytond.transaction import Transaction
from trytond.wizard import Button, StateAction, StateTransition, StateView, Wizard

from .queries import (
    lines_bank,
    lines_bank_no_transaction,
    schema_query_bank,
    schema_query_bank_transaction,
)

_STATES = {'readonly': Eval('state') != 'draft'}
_DEPENDS = ['state']
CONFIRMED_STATES = {
    'readonly': Not(Equal(Eval('state'), 'draft')),
    }
CONFIRMED_DEPENDS = ['state']

_ZERO = Decimal("0.0")


class BankStatement(Workflow, ModelSQL, ModelView):
    "Bank Statement"
    __name__ = 'account.bank_statement'
    # _rec_name = 'date'
    company = fields.Many2One('company.company', 'Company', required=True,
        states=_STATES, depends=['state'], domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', 0)),
            ])
    date = fields.Date('Date', required=True, states=_STATES,
        depends=['state'], help='Created date bank statement')
    bank_account = fields.Many2One('bank.account', 'Bank Account',
            required=True, states=_STATES, depends=['company', 'bank_accounts_company'])
    account = fields.Many2One('account.account', 'Account',
            required=True)
    bank = fields.Function(fields.Many2One('bank', 'Bank'),
            'on_change_with_bank')
    journal = fields.Many2One('account.journal', 'Journal',
            required=False, states=_STATES)
    period = fields.Many2One('account.period', 'Period', domain=[
            ('state', '=', 'open'),
            ('type', '=', 'standard'),
            ], states=_STATES)
    start_balance = fields.Numeric('Account Start Balance',
        required=True, digits=(16, 2), depends=['state'], states=_STATES)
    end_balance = fields.Function(fields.Numeric('Account End Balance',
        digits=(16, 2), states=_STATES,
        depends=['state']), 'get_end_balance')
    remainder = fields.Function(fields.Numeric('Remainder',
        digits=(16, 2), states=_STATES, depends=['state']),
        'get_remainder')
    total_remainder = fields.Function(fields.Numeric('Total Remainder',
        digits=(16, 2), states=_STATES, depends=['state', 'remainder']),
        'get_total_remainder')
    bank_start_balance = fields.Numeric('Bank Start Balance',
        required=True, digits=(16, 2), depends=['state'], states=_STATES)
    bank_end_balance = fields.Function(fields.Numeric('Bank End Balance',
        digits=(16, 2), depends=['lines']), 'get_bank_end_balance')
    lines = fields.One2Many('account.bank_statement.line', 'statement',
        'Lines', states=_STATES, context={
            'period': Eval('period'),
            'account': Eval('account'),
            'bank': Eval('bank'),
            }, depends=['period', 'account', 'bank'])
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ('canceled', 'Canceled'),
            ], 'State', required=True, readonly=True)
    lines_no_transaction = fields.Function(fields.Many2Many('account.move.line',
        None, None, 'Moves Lines without Transaction', domain=[
            ('period', '=', Eval('period')),
            ('account', '=', Eval('account')),
        ], order=[('date', 'ASC')]), 'get_lines_no_transaction')
    file_ = fields.Binary("File", states=_STATES)
    # bank_accounts_company = fields.Function(fields.One2Many('bank.account', 'bank', 'Bank Accounts', depends=['company']),
    #  'on_change_with_bank_accounts_company')

    @classmethod
    def __setup__(cls):
        super(BankStatement, cls).__setup__()
        cls._order.insert(0, ('period', 'DESC'))
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('confirmed', 'draft'),
                ('canceled', 'draft'),
                ('draft', 'canceled'),
                ))
        cls._buttons.update({
                'confirm': {
                    'invisible': ~Eval('state').in_(['draft']),
                    'icon': 'tryton-forward',
                    },
                'draft': {
                    'invisible': ~Eval('state').in_(['canceled', 'confirmed']),
                    'icon': 'tryton-clear',
                    },
                'cancel': {
                    'invisible': Eval('state').in_(['canceled']),
                    'icon': 'tryton-cancel',
                    },
                })

    # @fields.depends('company')
    # def on_change_with_bank_accounts_company(self):
    #     company_id = Transaction().context.get('company')
    #     Company = Pool().get('company.company')
    #     bank_accounts = None
    #     if company_id:
    #         company = Company(company_id)
    #         bank_accounts = [b.id for b in company.party.bank_accounts if company.party.bank_accounts]
    #     print(bank_accounts)
    #     return bank_accounts

    @classmethod
    def validate(cls, statements):
        for statement in statements:
            if not statement.bank_account or not statement.bank_account.account:
                raise AccessError(
                    gettext('account_bank_statement.msg_missing_config_account'))

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date():
        return datetime.datetime.now()

    @staticmethod
    def default_start_balance():
        return Decimal('0.0')

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, statements):
        for statement in statements:
            for line in statement.lines:
                if line.state != 'confirmed':
                    raise AccessError(
                        gettext('account_bank_statement.msg_lines_not_confirmed'))
            if statement.total_remainder != _ZERO:
                raise AccessError(
                    gettext('account_bank_statement.msg_remainder_not_zero'))

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    def cancel(cls, statements):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, statements):
        pass

    @classmethod
    def delete(cls, statements):
        for statement in statements:
            if statement.lines:
                raise AccessError(
                    gettext('account_bank_statement.msg_cannot_delete_statement', statement=statement.rec_name))
        super(BankStatement, cls).delete(statements)

    @fields.depends('bank_account')
    def on_change_with_bank(self, name=None):
        if self.bank_account:
            return self.bank_account.bank.id

    @fields.depends('bank_account')
    def on_change_bank_account(self, name=None):
        if self.bank_account and self.bank_account.account:
            self.account = self.bank_account.account.id

    def get_bank_end_balance(self, name):
        if not self.file_:
            res = sum([l.moves_amount for l in self.lines])
        else:
            res = sum(l.bank_amount for l in self.lines if l.bank_amount)
        if self.bank_start_balance:
            res += self.bank_start_balance
        return res

    def get_remainder(self, name):
        return (self.bank_end_balance - self.end_balance)

    def get_total_remainder(self, name):
        val = sum([l.debit - l.credit for l in self.lines_no_transaction])
        return (val + self.remainder)

    def get_end_balance(self, name):
        balance = self.start_balance or Decimal(0)
        MoveLine = Pool().get('account.move.line')
        period_balance = 0
        if self.period and self.bank_account and self.bank_account.account:
            move_lines = MoveLine.search([
                    ('period', '=', self.period.id),
                    ('account', '=', self.bank_account.account.id),
                ])
            period_balance = sum([l.debit - l.credit for l in move_lines])
        res = balance + period_balance
        return res

    def _search_update_lines(self, create=True):
        StatementLine = Pool().get('account.bank_statement.line')
        move_lines = self._search_related_lines()

        # Search current move lines inside statement lines
        current_move_lines = []
        for st_line in self.lines:
            current_move_lines.extend([l.id for l in st_line.bank_move_lines])

        st_lines_to_create = []
        for ml in move_lines:
            party_name = None
            description = None
            if ml.party and ml.party.name:
                party_name = ml.party.name
            if ml.id not in current_move_lines:
                if ml.description:
                    description = ml.description
                if party_name:
                    party_text = '[' + party_name + ']'
                    if description:
                        description = description + ' ' + party_text
                    else:
                        description = party_text
                if not description:
                    description = ml.account.name
                st_lines_to_create.append({
                    'statement': self.id,
                    'description': description,
                    'date': ml.move.date,
                    'state': 'draft',
                    'bank_move_lines': [('add', [ml.id])],
                })
        if create:
            StatementLine.create(st_lines_to_create)

    def get_lines_no_transaction(self, name):
        move_lines = set(ln.id for ln in self._search_related_lines())
        BankReconciliation = Pool().get('bank_statement.line-account.move.line')
        lines_reconciled = BankReconciliation.search([
            ('bank_statement_line.statement', '=', self.id)])
        bank_lines = set(ln.move_line.id for ln in lines_reconciled)
        return list(move_lines - bank_lines)

    def _search_related_lines(self):
        res = []
        if self.period and self.bank_account and self.bank_account.account:
            MoveLine = Pool().get('account.move.line')
            res = MoveLine.search([
                ('period', '=', self.period.id),
                ('account', '=', self.bank_account.account.id),
            ], order=[('date', 'asc')])
        return res

    def create_note(self):
        pool = Pool()
        Note = pool.get('account.note')
        context = Transaction().context
        party_id = self.bank.party.id
        concepts_bank = {c.concept: {'concept': c, 'amount': [], 'analytic_account': hasattr(c, 'analytic_account') and c.analytic_account} for c in self.bank.concepts if c.account}
        lines_concile = [line for line in self.lines if line.bank_concept in concepts_bank]
        for line in self.lines:
            if line.bank_concept in concepts_bank:
                concepts_bank[line.bank_concept]['amount'].append(line.bank_amount)

        lines_note = []
        balance = []
        for concept_bank in concepts_bank.values():
            amount = abs(sum(concept_bank['amount']))
            concept = concept_bank['concept']

            debit, credit = amount, 0
            if concept.debit_credit == 'credit':
                debit, credit = 0, amount

            value = {
                'description': concept.description,
                'account': concept.account,
                'reference': concept.concept,
                'debit': debit,
                'credit': credit,
                'party': party_id,
            }

            if concept_bank.get('analytic_account'):
                value['analytic_account'] = concept.analytic_account
            lines_note.append(value)
            balance.append(debit - credit)

        balance = sum(balance)
        debit, credit = 0, abs(balance)
        if balance < 0:
            debit, credit = abs(balance), 0
        line = {
            'account': self.bank_account.account.id,
            'debit': debit,
            'credit': credit,
            'party': party_id,

        }
        lines_note.append(line)

        note = {
            'company': context.get('company'),
            'date': self.period.end_date,
            'period': self.period,
            'journal': self.journal,
            'state': 'draft',
            'lines': [('create', lines_note)],
        }
        notes = Note.create([note])
        Note.post(notes)
        return notes[0], lines_concile

    def match_bank_transactions_with_moves(self, create_move=False, move_line=None):
        """
        Match bank transactions with moves
        """
        pool = Pool()
        BankReconciliation = pool.get("bank_statement.line-account.move.line")
        BankStatementLine = pool.get("account.bank_statement.line")

        concepts = [c.concept for c in self.bank.concepts if c.account]
        lines_concile = None
        to_create = []

        if move_line:
            lines_concile = [
                line for line in self.lines if line.bank_concept in concepts
            ]
        elif create_move:
            note, lines_concile = self.create_note()
            lines_bank_note = [
                ln.id for ln in note.move.lines if ln.account.id == self.bank_account.account.id
            ]
            move_line = lines_bank_note[0]

        if lines_concile and move_line:
            to_create = [
                {"move_line": move_line, "bank_statement_line": line.id}
                for line in lines_concile
            ]

        cursor = Transaction().connection.cursor()
        lines_bank_q = lines_bank.format(statement_id=self.id)
        lines_bank_no_transaction_q = lines_bank_no_transaction.format(
            period_id=self.period.id, account_id=self.bank_account.account.id,
        )

        df_bank = pl.read_database(
            lines_bank_q, cursor, schema_overrides=schema_query_bank,
        )
        df_move = pl.read_database(
            lines_bank_no_transaction_q,
            cursor,
            schema_overrides=schema_query_bank_transaction,
        )

        if not df_bank.is_empty() and not df_move.is_empty():
            df_bank = df_bank.with_columns(
                pl.col("bank_date")
                .cum_count()
                .over(["bank_date", "bank_amount"])
                .alias("sequence"),
            )
            df_bank = df_bank.with_columns(
                (
                    pl.col("bank_date").cast(pl.String)
                    + "_"
                    + pl.col("bank_amount").cast(pl.String)
                    + "_"
                    + pl.col("sequence").cast(pl.String)
                ).alias("key"),
            )

            df_move = df_move.with_columns(
                pl.col("date").cum_count().over(["date", "balance"]).alias("sequence"),
            )
            df_move = df_move.with_columns(
                (
                    pl.col("date").cast(pl.String)
                    + "_"
                    + pl.col("balance").cast(pl.String)
                    + "_"
                    + pl.col("sequence").cast(pl.String)
                ).alias("key"),
            )

            df_equal = df_bank.join(df_move, on="key", how="inner")

            # get lines match between bank and move rounding balance
            df_diff_bank = df_bank.join(df_move, on="key", how="anti")
            df_diff_move = df_move.join(df_bank, on="key", how="anti")

            df_diff_bank = df_diff_bank.with_columns(
                pl.col("bank_amount").cast(pl.Int64),
            )
            df_diff_bank = df_diff_bank.with_columns(
                (
                    pl.col("bank_date").cast(pl.String)
                    + "_"
                    + pl.col("bank_amount").cast(pl.String)
                    + "_"
                    + pl.col("sequence").cast(pl.String)
                ).alias("key"),
            )

            df_diff_move = df_diff_move.with_columns(
                (
                    pl.col("date").cast(pl.String)
                    + "_"
                    + pl.col("balance").ceil().cast(pl.Int64).cast(pl.String)
                    + "_"
                    + pl.col("sequence").cast(pl.String)
                ).alias("key"),
            )

            df_equal_from_int = df_diff_bank.join(df_diff_move, on="key", how="inner")

            # get lines match between bank and move without truncating balance
            df_diff_bank_int = df_diff_bank.join(df_diff_move, on="key", how="anti")

            move_ids = df_equal_from_int["move_id"].to_list() + df_equal["move_id"].to_list()
            df_diff_move_int = (
                df_move.filter(~pl.col("move_id").is_in(move_ids))
                .select(["move_id", "date", "balance", "sequence"])
                .with_columns(
                    (
                        pl.col("date").cast(pl.String)
                        + "_"
                        + pl.col("balance").cast(pl.Int64).cast(pl.String)
                        + "_"
                        + pl.col("sequence").cast(pl.String)
                    ).alias("key"),
                )
            )

            df_equal_from_trunc = df_diff_bank_int.join(df_diff_move_int, on="key", how="inner")

            for row in df_equal.rows():
                to_create.append({"bank_statement_line": row[0], "move_line": row[5]})
            for row in df_equal_from_int.rows():
                to_create.append({"bank_statement_line": row[0], "move_line": row[5]})
            for row in df_equal_from_trunc.rows():
                to_create.append({"bank_statement_line": row[0], "move_line": row[5]})
            BankReconciliation.create(to_create)

            lines_confirmed = (
                df_equal["line_id"]
                .extend(df_equal_from_int["line_id"])
                .unique()
                .to_list()
            )
            lines = BankStatementLine.browse(lines_confirmed)
            if lines_concile:
                lines.extend(lines_concile)
            BankStatementLine.write(lines, {"state": "confirmed"})


class BankStatementLine(Workflow, ModelSQL, ModelView):
    "Bank Statement Line"
    __name__ = 'account.bank_statement.line'
    _rec_name = 'description'
    statement = fields.Many2One('account.bank_statement', 'Bank Statement',
        required=True)
    date = fields.Date('Date', states=CONFIRMED_STATES)
    sequence = fields.Integer('Sequence', states=CONFIRMED_STATES)
    description = fields.Char('Description',
        states=CONFIRMED_STATES)
    bank_description = fields.Char('Bank Description', states={'readonly': True})
    bank_date = fields.Date('Bank Date', states={'readonly': True})
    bank_concept = fields.Char('Bank Concept', states={'readonly': True})
    bank_amount = fields.Numeric('Bank Amount', digits=(16, 2), states={'readonly': True})
    state = fields.Selection([
            ('draft', 'Draft'),
            ('confirmed', 'Confirmed'),
            ], 'State', readonly=True)
    bank_move_lines = fields.Many2Many('bank_statement.line-account.move.line',
        'bank_statement_line', 'move_line', 'Bank Move Line',
            states=CONFIRMED_STATES, depends=['state', 'statement'],
                domain=[
                    ('account', '=', Eval('_parent_statement', {}).get('account')),
                ])
    moves_amount = fields.Function(fields.Numeric('Moves Amount',
            digits=(16, 2), depends=['amount']), 'get_moves_amount')
    transactions = fields.One2Many('account.bank_statement.transaction',
            'line', 'Transactions', states=CONFIRMED_STATES,
        context={
                'date': Eval('date'),
                'description': Eval('description'),
            }, depends=['statement'])

    @classmethod
    def __setup__(cls):
        super(BankStatementLine, cls).__setup__()
        cls._order.insert(0, ('bank_date', 'ASC'))
        cls._order.insert(1, ('date', 'ASC'))
        cls._transitions |= set((
                ('draft', 'confirmed'),
                ('confirmed', 'draft'),
                ))
        cls._buttons.update({
                'confirm': {
                    'invisible': Eval('state') == 'confirmed',
                    },
                'draft': {
                    'invisible': Eval('state') == 'draft',
                    },
                'search_reconcile': {
                    'invisible': Eval('state') == 'confirmed',
                    },
                })

    @classmethod
    def __register__(cls, module_name):
        super(BankStatementLine, cls).__register__(module_name)
        table = cls.__table_handler__(module_name)

        # Migration from 7.0: drop not null on journal and description
        table.not_null_action('date', 'remove')
        table.not_null_action('description', 'remove')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date():
        res = None
        period_id = Transaction().context.get('period')
        Period = Pool().get('account.period')
        if period_id:
            period = Period(period_id)
            res = period.start_date
        return res

    def get_moves_amount(self, name):
        res = sum([(ml.debit - ml.credit) for ml in self.bank_move_lines])
        return res

    @classmethod
    @ModelView.button_action('account_bank_statement.wizard_search_lines_unreconciled')
    def search_reconcile(cls, st_lines):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, lines):
        for line in lines:
            for t in line.transactions:
                move = t.create_move()
                if move:
                    t.write([t], {'move': move})
            if not line.bank_move_lines:
                raise AccessError(
                    gettext('account_bank_statement.msg_not_bank_move_lines'))

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, lines):
        pass

    @classmethod
    def search(cls, args, offset=0, limit=None, order=None, count=False,
            query=False):
        """
        Override default search function so that if the user sorts by one field
        only, then 'sequence' is always added as a second sort field. This is
        specially important when the user sorts by date (the most usual) to
        ensure all moves from the same date are properly sorted.
        """
        if order is None:
            order = []
        order = list(order)
        if len(order) == 1:
            order.append(('sequence', order[0][1]))
        return super(BankStatementLine, cls).search(args, offset, limit, order,
            count, query)


class BankReconciliation(ModelSQL):
    "Bank Reconciliation"
    __name__ = 'bank_statement.line-account.move.line'
    _table = 'bank_statement_line_account_move_line'
    bank_statement_line = fields.Many2One('account.bank_statement.line',
        'Bank Statement Line', ondelete='CASCADE',
        required=True)
    move_line = fields.Many2One('account.move.line', 'Move Line',
        required=True, ondelete='RESTRICT')


class BankStatementTransaction(ModelSQL, ModelView):
    "Bank Statement Transaction"
    __name__ = 'account.bank_statement.transaction'
    line = fields.Many2One('account.bank_statement.line', 'Line',
        required=True, ondelete='CASCADE')
    date = fields.Date('Date', required=True)
    amount = fields.Numeric('Amount', required=True, digits=(16, 2))
    party = fields.Many2One('party.party', 'Party')
    account = fields.Many2One('account.account', 'Account', required=True,
        domain=[('type', '!=', None)], depends=['line'])
    description = fields.Char('Description')
    move = fields.Many2One('account.move', 'Account Move', readonly=True)

    @classmethod
    def __setup__(cls):
        super(BankStatementTransaction, cls).__setup__()
        """
        # FIXME: Use python-sql
        cls._sql_constraints += [(
                'check_bank_move_amount', 'CHECK(amount != 0)',
                'Amount should be a positive or negative value.'),
        ]
        """

    @staticmethod
    def default_party():
        context = Transaction().context
        Bank = Pool().get('bank')
        if 'bank' in context:
            return Bank(context['bank']).party.id

    @staticmethod
    def default_account():
        context = Transaction().context
        Bank = Pool().get('bank')
        if 'bank' in context:
            bank = Bank(context['bank'])
            if bank.account_expense:
                return bank.account_expense.id

    @staticmethod
    def default_description():
        context = Transaction().context
        if 'description' in context:
            return Transaction().context.get('description')

    @staticmethod
    def default_date():
        if Transaction().context.get('date'):
            return Transaction().context.get('date')
        return None

    @fields.depends('account', 'amount', 'party')
    def on_change_party(self):
        if self.party and self.amount:
            if self.amount > Decimal("0.0"):
                account = self.account or self.party.account_receivable
            else:
                account = self.account or self.party.account_payable
            self.account = account.id

    @fields.depends('amount', 'party', 'account')
    def on_change_amount(self):
        if self.party and not self.account and self.amount:
            if self.amount > Decimal("0.0"):
                account = self.party.account_receivable
            else:
                account = self.party.account_payable
            self.account = account.id

    def get_rec_name(self, name):
        return self.line.rec_name

    @classmethod
    def copy(cls, lines, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default.setdefault('move', None)
        return super(BankStatementTransaction, cls).copy(lines, default=default)

    def create_move(self):
        """
        Create move for the statement line and return move if created.
        """
        pool = Pool()
        Move = pool.get('account.move')
        BankReconciliation = pool.get('bank_statement.line-account.move.line')

        if self.move:
            return

        lines_to_create = self._create_move_lines()
        values = {
            'company': self.line.statement.company.id,
            'period': self.line.statement.period.id,
            'journal': self.line.statement.journal.id,
            'date': self.date,
            'lines': [('create', lines_to_create)],
            'description': self.description,
        }
        move, = Move.create([values])

        lines_to_reconcile = [ml.id for ml in move.lines if ml.account.id == self.line.statement.account.id]
        for ln in lines_to_reconcile:
            BankReconciliation.create([{
                    'move_line': ln,
                    'bank_statement_line': self.line.id,
            }])
        move.save()
        return move

    @classmethod
    def post_move(cls, lines):
        Move = Pool().get('account.move')
        Move.post([l.move for l in lines if l.move])

    @classmethod
    def delete_move(cls, lines):
        Move = Pool().get('account.move')
        Move.delete([l.move for l in lines if l.move])

    def _create_move_lines(self):
        """
        Return the move lines for the statement line
        """
        amount = self.amount

        if not self.line.statement.account.bank_reconcile:
            raise AccessError(
                gettext('account_bank_statement.msg_account_not_reconcilable', self.line.statement.account.rec_name))
        if self.account.id == self.line.statement.account.id:
            raise AccessError(
                gettext('account_bank_statement.msg_same_debit_credit_account',
                        account=self.line.statement.account.rec_name,
                        line=self.account.rec_name))

        product_line = {
                'description': self.description,
                'debit': (amount < _ZERO and -amount) or _ZERO,
                'credit': (amount >= _ZERO and amount) or _ZERO,
                'account': self.account.id,
        }
        if self.account.party_required:
            product_line['party'] = self.party.id

        bank_line = {
            'description': self.description,
            'debit': (amount >= _ZERO and amount) or _ZERO,
            'credit': (amount < _ZERO and -amount) or _ZERO,
            'account': self.line.statement.account.id,
        }

        if self.line.statement.account.party_required:
            product_line['party'] = self.party.id
        return [bank_line, product_line]


class UpdateBankStatementLinesStart(ModelView):
    "Update Bank Statement Lines (Start)"
    __name__ = 'account.bank_statement.update_lines.start'

    statement = fields.Many2One('account.bank_statement', 'Bank Statement')
    create_move = fields.Boolean('Create Move')
    move_line = fields.Many2One('account.move.line', 'Move Line', domain=[
        ('move.period', '=', Eval('period')),
        ('account', '=', Eval('account')),
    ])
    account = fields.Many2One('account.account', 'Account', states={
        'readonly': True,
        })
    period = fields.Many2One('account.period', 'Period', states={
        'readonly': True,
        })


class UpdateBankStatementLines(Wizard):
    "Update Bank Statement Lines"
    __name__ = 'account.bank_statement.update_lines'
    start_state = 'update_lines'
    update_lines = StateTransition()

    state_confirm = StateView('account.bank_statement.update_lines.start',
        'account_bank_statement.update_lines_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Update', 'match', 'tryton-ok', default=True),
        ])

    match = StateTransition()

    def transition_update_lines(self):
        BankStatement = Pool().get('account.bank_statement')
        ids = Transaction().context['active_ids']
        if ids:
            id_ = ids[0]
            statement = BankStatement(id_)
            if statement.state == 'draft' and not statement.file_:
                statement._search_update_lines()
            else:
                return 'state_confirm'
        return 'end'

    def default_state_confirm(self, fields):
        BankStatement = Pool().get('account.bank_statement')
        statement = BankStatement(Transaction().context['active_id'])
        return {
            'statement': statement.id,
            'move_line': None,
            'period': statement.period.id,
            'account': statement.account.id,
        }

    def transition_match(self):
        BankStatement = Pool().get('account.bank_statement')
        statement = BankStatement(Transaction().context['active_id'])
        statement.match_bank_transactions_with_moves(
            create_move=self.state_confirm.create_move,
            move_line=self.state_confirm.move_line)
        return 'end'


class SearchBankUnreconciledsLinesStart(ModelView):
    "Search Bank Unreconciled Lines (Start)"
    __name__ = 'account_bank_statement.search_bank_unreconciled_lines.start'
    move_lines = fields.Many2Many('account.move.line', None, None, 'Move Line', required=True,
        domain=[
            ('account', '=', Eval('account')),
            ('bank_lines', '=', None),
            ])
    account = fields.Many2One('account.account', 'Account',
            domain=[('kind', '!=', 'view')])

    @staticmethod
    def default_account():
        pool = Pool()
        StLine = pool.get('account.bank_statement.line')
        st_line_id = Transaction().context.get('active_id')
        st_line = StLine(st_line_id)
        return st_line.statement.account.id


class SearchBankUnreconciledLines(Wizard):
    "Search Bank Unreconciled Lines"
    __name__ = 'account_bank_statement.search_bank_unreconciled_lines'

    start = StateView('account_bank_statement.search_bank_unreconciled_lines.start',
        'account_bank_statement.search_bank_lines_unreconciled_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Add', 'search_', 'tryton-ok', default=True),
            ])
    search_ = StateTransition()

    def transition_search_(self):
        pool = Pool()
        Line = pool.get('account.bank_statement.line')
        line = Line(Transaction().context.get('active_id'))
        for move_line in self.start.move_lines:
            BankReconciliation.create([{
                    'move_line': move_line,
                    'bank_statement_line': line.id,
                    }])
        return 'end'


class CreateBankStatementStart(ModelView):
    "Create Bank Statement Start"
    __name__ = 'account_bank_statement.create_bank_statement.start'
    company = fields.Many2One('company.company', "Company", required=True)

    file_ = fields.Binary("File")
    bank_account = fields.Many2One('bank.account', 'Bank Account',
        domain=[('owners.id', '=', Eval('party', -1))], required=True,
        depends=['party', 'company'])
    bank = fields.Selection('get_banks', 'Bank')
    period = fields.Many2One('account.period', 'Period', domain=[
        ('company', '=', Eval('company', -1)),
        ('state', '!=', 'close'),
        ], required=True)
    party = fields.Function(fields.Many2One('party.party', 'Party'),
        'get_party', 'eager')

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @staticmethod
    def default_period():
        Period = Pool().get('account.period')
        date_ = datetime.date.today() - relativedelta(months=1)
        return Period.find(Transaction().context.get('company'), date=date_).id

    @classmethod
    def get_banks(cls):
        return [
            ('', ''),
            ('bancolombia', 'Bancolombia'),
            # ('davivienda', 'Davivienda'),
            # ('bbva', 'BBVA'),
        ]

    @fields.depends('company', 'party')
    def on_change_company(self):
        Company = Pool().get('company.company')
        company = Company(Transaction().context.get('company'))
        self.party = company.party.id


class CreateBankStatement(Wizard):
    "Create Bank Statement"
    __name__ = 'account_bank_statement.create_bank_statement'
    start = StateView('account_bank_statement.create_bank_statement.start',
        'account_bank_statement.create_bank_statement_view_form', [
                Button('Cancel', 'end', 'tryton-cancel'),
                Button('Add', 'import_', 'tryton-ok', default=True),
            ])
    import_ = StateAction('account_bank_statement.act_bank_statement')

    def do_import_(self, action):
        if self.start.file_:
            lines_statement = list(getattr(self, f'parse_{self.start.bank}')())
            bank_statements = self.create_statement(lines_statement)
        else:
            bank_statements = self.create_statement_without_file_statement()
        print(bank_statements, 'bank_statements')
        data = {'res_id': list(map(int, bank_statements))}
        if len(bank_statements) == 1:
            action['views'].reverse()
        return action, data

    def create_statement(self, lines):
        pool = Pool()
        value = self.get_bank_statement_to_create()
        value['lines'] = [('create', lines)]
        # value['bank_end_balance'] = balance
        value['file_'] = self.start.file_
        BankStatement = pool.get('account.bank_statement')
        bank_statements = BankStatement.create([value])
        # bank_statements[0]._search_update_lines()
        return bank_statements

    def parse_bancolombia(self):
        file_ = self.start.file_.decode('utf-8')
        reader = file_.splitlines()

        def parse_line(line):
            """Parsea una línea del archivo .txt y devuelve una tupla con los datos extraídos."""
            pattern = r"(\d{11})(\d{4})(\d{8})(\d{9})(-?\d{12,13}\.\d{2})(\d{4})(.*)(\d{2})"
            match = re.match(pattern, line)
            return match.groups()

        lines = []
        for line in reader:
            line_ = parse_line(line)
            value = {
                'state': 'draft',
                'bank_description': line_[6].strip(),
                'bank_concept': line_[5],
                'bank_date': datetime.datetime.strptime(line_[2], "%d%m%Y").date(),
                'bank_amount': Decimal(float(line_[4])).quantize(Decimal('0.01')),
            }
            lines.append(value)

        return lines

    def create_statement_without_file_statement(self):
        value = self.get_bank_statement_to_create()
        bank_statements = BankStatement.create([value])
        bank_statements[0]._search_update_lines()
        return bank_statements

    def get_bank_statement_to_create(self):
        pool = Pool()
        BankStatement = pool.get('account.bank_statement')
        Configuration = pool.get('account.bank_statement.configuration')

        value = {
                'bank_account': self.start.bank_account.id,
                'account': self.start.bank_account.account.id,
                'date': self.start.period.start_date,
                'start_balance': _ZERO,
                'bank_start_balance': _ZERO,
                }

        if Configuration(1).default_journal_bank:
            journal_id = Configuration(1).default_journal_bank.id
            value['journal'] = journal_id

        current_bank_statements = BankStatement.search([
                ('bank_account', '=', self.start.bank_account.id),
                ], order=[('date', 'DESC')])
        if current_bank_statements:
            value['bank_start_balance'] = current_bank_statements[0].bank_end_balance
        value['period'] = self.start.period.id
        return value
