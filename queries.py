import polars as pl

schema_query_bank = {
    'line_id': pl.Int64,
    'bank_date': pl.Date,
    'bank_amount': pl.Float32,
}

lines_bank = """
    SELECT
        l.id as line_id,
        l.bank_date,
        l.bank_amount
    FROM account_bank_statement_line as l
    WHERE l.statement={statement_id} and l.state != 'confirmed'
    ORDER BY l.bank_date, l.bank_amount
"""

lines_bank_no_transaction = """
    SELECT
        m.id as move_id, a.date,
        m.account, m.debit,
        m.credit,
        m.debit - m.credit AS balance
    FROM account_move_line as m
    LEFT JOIN account_move as a on a.id=m.move
    LEFT JOIN "bank_statement_line_account_move_line" as s on s.move_line=m.id
    WHERE a.period={period_id} AND m.account={account_id} AND s.move_line IS NULL
    ORDER BY a.date, m.debit - m.credit
"""

schema_query_bank_transaction = {
    'move_id': pl.Int64,
    'date': pl.Date,
    'account': pl.Int64,
    'debit': pl.Float32,
    'credit': pl.Float32,
    'balance': pl.Float32,
}
